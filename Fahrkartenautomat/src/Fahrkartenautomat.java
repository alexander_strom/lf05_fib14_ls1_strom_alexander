﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;

       // Erfassung der Bestellung
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       // Bezahlung der Fahrkarten
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       // Ausgabe der Fahrkarten
       fahrkartenAusgeben();
       
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    }
    
    // Erfasst die Fahrkartenbestellung und gibt den Gesamtpreis der Fahrkarten zurück
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	// Notwendige Variablen deklarieren
        int anzahlTickets;
        double ticketPreis;
        double zuZahlenderBetrag;
        
        // Abfrage: Preis pro Ticket
        System.out.print("Ticketpreis (Euro):");
        ticketPreis = tastatur.nextDouble();
        
        // Abfrage: Anzahl der Tickets
        System.out.print("Anzahl der Tickets:");
        anzahlTickets = tastatur.nextInt();
        
        // Berechnung: zu zahlender Betrag:
        zuZahlenderBetrag = anzahlTickets * ticketPreis;
        return zuZahlenderBetrag;
    	
        
    	
    }
    
    // Erfasst den Geldeinwurf und gibt den eingeworfenen Betrag zurück
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
        
    	// Notwendige Variablen deklarieren (und initialisieren)
    	double eingeworfeneMünze;
    	double eingezahlterGesamtbetrag = 0.0; 	
    	
        // Geldeinwurf
        // -----------
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
        return eingezahlterGesamtbetrag;
    }
    
    // Ausgabe der Fahrkarten
    public static void fahrkartenAusgeben() {

        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
        
        System.out.println("                                                            ");
        System.out.println("                ..                         ..               ");
        System.out.println("        .,,,,,,,,,,,,,,,,           ,,,,,,,,,,,,,,,,        ");
        System.out.println("     .,,,,,,,,,,,,,,,,,,,,,,     ,,,,,,,,,,,,,,,,,,,,,,     ");
        System.out.println("   .,,,,,,,,,,,,,,,,,,,,,,,,,,.,,,,,,,,,,,,,,,,,,,,,,,,,,   ");
        System.out.println("  ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, ");
        System.out.println(" ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
        System.out.println(" ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
        System.out.println(",,,,,,,,,,,,,,,,,&&&&&&&,,&&/,,,@&&,,(&&&&&&.,,,,,,,,,,,,,,,");
        System.out.println(" ,,,,,,,,,,,,,,,,&&#,,&&*,&&&,,,&&.,&&@,,,,,,,,,,,,,,,,,,,,,");
        System.out.println(" ,,,,,,,,,,,,,,,,&&&&&&*,,,&&(,@&&,.&&,,&&&@,,,,,,,,,,,,,,,,");
        System.out.println("  ,,,,,,,,,,,,,,,&&#,,&&@,,(&&,&&,,.&&*,,,&&*,,,,,,,,,,,,,, ");
        System.out.println("   .,,,,,,,,,,,,,&&#,,&&@,,,&&&&%,,,&&&,,,&&*,,,,,,,,,,,,   ");
        System.out.println("     .,,,,,,,,,,,%&&&%*,,,,,,&&(,,,,,,(@@@%,,,,,,,,,,,,     ");
        System.out.println("        ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,       ");
        System.out.println("          ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,.         ");
        System.out.println("             ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,            ");
        System.out.println("               ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,.              ");
        System.out.println("                  ,,,,,,,,,,,,,,,,,,,,,,,,,                 ");
        System.out.println("                    ,,,,,,,,,,,,,,,,,,,,,                   ");
        System.out.println("                       ,,,,,,,,,,,,,,,                      ");
        System.out.println("                         ,,,,,,,,,,,                        ");
        System.out.println("                           .,,,,,                           ");
        System.out.println("                            ,,,,                            ");
        System.out.println("                             ,,                             ");
        System.out.println("                                                            ");
    }
    
    // Berechnung und Ausgabe des Rückgelds
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rückgabebetrag;
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
     }
}