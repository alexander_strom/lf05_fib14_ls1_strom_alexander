package Ausgabeformatierung_1;

public class DritteAufgabe {

	public static void main(String[] args) {
		double[] alleZahlen = {22.4234234,111.2222, 4.0, 1000000.551, 97.34};
		
		for(double zahl : alleZahlen) {
			System.out.printf("%.2f\n", zahl);
		}
	}

}
