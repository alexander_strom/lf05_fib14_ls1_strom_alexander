package Ausgabeformatierung_1;

public class ErsteAufgabe {

	public static void main(String[] args) {
		// Aufgabe 1:
		String ersterSatz = "Das Programm rief: \"Hallo Welt\".";
		String zweiterSatz = "Das hier ist der zweite Satz.";
		
		System.out.print(ersterSatz + "\n" + zweiterSatz);
		
		// Die println()-Anweisung fügt am Ende der Ausgabe einen Zeilenumbruch (\n) hinzu. Bei der print()-Anweisung gibt es keinen Zeilenumbruch am Ende, daher stehen die Texte bei der Verwendung von print() nebeneinander und nicht unteraneinander, wie bei println().	
	}

}
