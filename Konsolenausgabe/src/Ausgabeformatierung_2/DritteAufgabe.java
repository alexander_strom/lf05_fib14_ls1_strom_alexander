package Ausgabeformatierung_2;

public class DritteAufgabe {

	public static void main(String[] args) {
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");
		System.out.printf("%23s\n", "-----------------------");
		System.out.printf("%+-12d|%+10.2f\n", -20, -28.8889);
		System.out.printf("%+-12d|%+10.2f\n", -10, -23.3333);
		System.out.printf("%+-12d|%+10.2f\n", 0, -17.7778);
		System.out.printf("%+-12d|%+10.2f\n", 20, -6.6667);
		System.out.printf("%+-12d|%+10.2f\n", 30, -1.1111);

// Automatische Umrechnung der Temperatur, funktioniert noch nicht
//		int fahrenheitArr[] = {-20, -10, 0, 20, 30};
//		for(int fahrenheit : fahrenheitArr) {
//			double celsius = (fahrenheit-32)*5/9;
//			System.out.printf("%+-12d|%+10.2f\n", fahrenheit, celsius);
//		}
	}

}
