
public class Aufgabe1 {

	public static void main(String[] args) {
		double wert1 = 10.0;
		double wert2 = 20.0;
		
		double m = berechneMittelwert(wert1, wert2);
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", wert1, wert2, m);
		
	}
	
	//	Berechnet den Mittelwert aus zwei übergebenen Double-Werten
	public static double berechneMittelwert(double x, double y){
		return (x + y) / 2.0;
	}

}
