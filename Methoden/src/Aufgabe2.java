import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		// EVA-Prinzip
		
		// Eingabe:
		String artikel = liesString("Was möchten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double nettopreis = liesDouble("Geben Sie den Nettopreis pro Artikel ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		// Verarbeitung:
		double nettoGesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		double bruttoGesamtpreis = berechneGesamtbruttopreis(nettoGesamtpreis, mwst);
		
		// Ausgabe:
		rechnungAusgeben(artikel, anzahl, nettoGesamtpreis, bruttoGesamtpreis, mwst);
		
	}
	
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println(text);
		return myScanner.next();
	}
	
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println(text);
		return myScanner.nextInt();
	}
	
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println(text);
		return myScanner.nextDouble();
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}
	
	public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
