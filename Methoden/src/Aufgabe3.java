import java.util.Scanner;

public class Aufgabe3 {

	public static void main(String[] args) {
		// Eingabe:
		double r1 = liesDouble("Geben Sie den ersten Teilwiderstand ein:");
		double r2 = liesDouble("Geben Sie den zweiten Teilwiderstand ein:");
		
		//	Verarbeitung:
		double ersatzwiderstand = reihenschaltung(r1, r2);
		
		//	Ausgabe:
		System.out.printf("Der Ersatzwiderstand von %.2f Ohm und %.2f Ohm beträgt %.2f Ohm.", r1, r2, ersatzwiderstand);
	}

	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println(text);
		return myScanner.nextDouble();
	}
	
	public static double reihenschaltung(double r1, double r2) {
		return r1 + r2;
	}
}
