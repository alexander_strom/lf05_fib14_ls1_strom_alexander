package Aufgabe5;

import java.util.Scanner;

public class Mathe {

	public static void main(String[] args) {
		// Eingabe:
		double eingabe = liesDouble("Geben Sie den zu quadrierenden Wert ein:");
		
		// Verarbeitung:
		double ergebnis = quadrat(eingabe);
		
		//	Ausgabe:
		System.out.printf("Die Quadratsumme von %f ist %f", eingabe, ergebnis);
	}

	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println(text);
		return myScanner.nextDouble();
	}
	
	public static double quadrat(double x) {
		return x * x;
	}
}
